using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Org.BouncyCastle.Tsp;
using Org.BouncyCastle.Math;
using System.Security.Cryptography;
using System.Net;
using System.IO;
using Org.BouncyCastle.Asn1.Cmp;
using Org.BouncyCastle.Asn1.X509;

namespace CarimboBC
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //Calculando o resumo criptografico SHA-256 do conteudo que sera carimbado
			SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
           	byte[] imprint = sha256.ComputeHash(Encoding.ASCII.GetBytes("Teste".ToCharArray()));
			
			//Enviando requisição de carimbo do tempo
            byte[] carimbo = GetTimestampToken("https://cloud.bry.com.br/carimbo-do-tempo/tsp", "CLIENT_ID", "CLIENT_SECRET", imprint);

            //Conversão do retorno em um arquivo .tsr         
            if (carimbo.Length > 0)
            {
                Console.WriteLine(Convert.ToBase64String(carimbo));

                if (ByteArrayToFile("<CAMINHO-PARA-ESCREVER-O-ARQUIVO>/carimbo.tsr", carimbo))
                    Console.WriteLine("Arquivo carimbo salvo com sucesso.");
                Console.ReadLine();
            }
        }

        static public byte[] GetTimestampToken(String tsaURL, string tsaUserName, string tsaPassword, byte[] imprint)
        {
            TimeStampRequestGenerator tsaGenerator = new TimeStampRequestGenerator();
            tsaGenerator.SetCertReq(true);
			
            //Definido o OID da politica
			tsaGenerator.SetReqPolicy("2.16.76.1.6.6");

			//OID SHA-256
            TimeStampRequest request = tsaGenerator.Generate("2.16.840.1.101.3.4.2.1", imprint, BigInteger.ValueOf(DateTime.Now.Ticks));
            byte[] requestBytes = request.GetEncoded();

            byte[] responseBytes = GetTSAResponse(tsaURL, tsaUserName, tsaPassword, requestBytes);
            
            TimeStampResponse response = new TimeStampResponse(responseBytes);
            
            PkiFailureInfo failure = response.GetFailInfo();
            int value = (failure == null) ? 0 : failure.IntValue;
            if (value != 0)
            {
                Console.WriteLine("Status (PKIStatus): " + response.Status);
                Console.WriteLine(failure.ToString());
                Console.WriteLine("Mensagem de erro: " + response.GetStatusString());

                throw new IOException(String.Format("invalid.tsa.1.response.code.2", tsaURL, value.ToString()));
            }
            TimeStampToken tsToken = response.TimeStampToken;
            if (tsToken == null)
            {
                throw new IOException(String.Format("tsa.1.failed.to.return.time.stamp.token.2", tsaURL, value.ToString()));
            }

            response.Validate(request);

            return tsToken.GetEncoded();
        }

        static protected byte[] GetTSAResponse(String tsaURL, string tsaUserName, string tsaPassword, byte[] requestBytes)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            Uri uri = new Uri(tsaURL);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.ProtocolVersion = HttpVersion.Version10;
            request.ContentType = "application/timestamp-query";
            request.ContentLength = requestBytes.Length;
            request.Method = "POST";
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes(tsaUserName + ":" + tsaPassword)));

            Stream requestStream = null;
            try
            {
                requestStream = request.GetRequestStream();
                requestStream.Write(requestBytes, 0, requestBytes.Length);
            }
            finally
            {
                requestStream.Close();
            }

            WebResponse webResponse = null;
            Stream responseStream = null;
            byte[] responseBytes;

            try
            {
                webResponse = request.GetResponse();
                responseStream = webResponse.GetResponseStream();
                responseBytes = ReadFully(responseStream);

            } finally
            {
                responseStream.Close();
            }
            return responseBytes;

        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        public static bool ByteArrayToFile(string _FileName, byte[] _ByteArray)
        {
            try
            {
                System.IO.FileStream _FileStream = new System.IO.FileStream(_FileName, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(_ByteArray, 0, _ByteArray.Length);
                _FileStream.Close();
                return true;
            }
            catch (Exception _Exception)
            {
                Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
            }

            return false;
        }
    }
}
