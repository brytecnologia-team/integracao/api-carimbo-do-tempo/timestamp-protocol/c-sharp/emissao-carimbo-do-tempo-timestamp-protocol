# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2020-11-26

### Added

- Examples of timestamp issue. 

[1.0.0]: https://gitlab.com/brytecnologia-team/integracao/api-carimbo-do-tempo/c-sharp/emissao-carimbo-do-tempo-timestamp-protocol/-/tags/1.0.0

## [1.0.1] - 2022-04-25

### Added

- Changed certificate setting to true. 

[1.0.1]: https://gitlab.com/brytecnologia-team/integracao/api-carimbo-do-tempo/c-sharp/emissao-carimbo-do-tempo-timestamp-protocol/-/tags/1.0.1

## [1.0.2] - 2022-05-04

### Added

- Added instructions to print error status and detail to the console.

[1.0.2]: https://gitlab.com/brytecnologia-team/integracao/api-carimbo-do-tempo/c-sharp/emissao-carimbo-do-tempo-timestamp-protocol/-/tags/1.0.2
